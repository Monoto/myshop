﻿using MyShop.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace MyShop.DataAccess.InMemory
{
  public class ProductCategoryRepository
  {
    ObjectCache cache = MemoryCache.Default;
    List<ProductCategory> productsCategories;

    public ProductCategoryRepository()
    {
      productsCategories = cache["productsCat"] as List<ProductCategory>;
      if (productsCategories == null)
      {
        productsCategories = new List<ProductCategory>();
      }
    }

    public void Commit()
    {
      cache["productsCat"] = productsCategories;
    }

    public void Insert(ProductCategory p)
    {
      productsCategories.Add(p);
    }

    public void Update(ProductCategory product)
    {
      ProductCategory productCatagoryToUpdate = productsCategories.Find(p => p.Id == product.Id);
      if (productCatagoryToUpdate != null)
      {
        productCatagoryToUpdate = product;
      }
      else
      {
        throw new Exception("Product not found");
      }
    }

    public ProductCategory Find(string Id)
    {
      ProductCategory productsCategory = productsCategories.Find(p => p.Id == Id);
      if (productsCategory != null)
      {
        return productsCategory;
      }
      else
      {
        throw new Exception("Product not found");
      }
    }

    public IQueryable<ProductCategory> Collection()
    {
      return productsCategories.AsQueryable();
    }

    public void Delete(string Id)
    {
      ProductCategory productCatagoryToDelete = productsCategories.Find(p => p.Id == Id);
      if (productCatagoryToDelete != null)
      {
        productsCategories.Remove(productCatagoryToDelete);
      }
      else
      {
        throw new Exception("Product not found");
      }
    }

  }
}

